Rough and ready scripts to turn GeoTIFFs of LINZ topo maps into a tiled
[GEMF](https://www.cgtk.co.uk/gemf) file, which gave better offline raster map
performance than MBTiles in [Locus Map](https://www.locusmap.app/).

Written over a few days prior to a road trip where I wanted offline topo maps.
Will refactor one of these days to make it a bit more usable, but it did the
job at the time =).