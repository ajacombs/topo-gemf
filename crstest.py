from pathlib import Path
import rasterio

SRCDIR_250 = Path('/Volumes/Tortoise/Topo/250')
SRCDIR_50 = Path('/Volumes/Tortoise/Topo/50')

for sheet in SRCDIR_250.iterdir():
    rio = rasterio.open(str(sheet))
    print('Sheet: {}, CRS: {}'.format(sheet.stem, rio.crs))
