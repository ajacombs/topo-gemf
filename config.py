import yaml
from pathlib import Path

with open('nztopo.config.yml') as f:
    config = yaml.load(f)
    for item in config['files']:
        for collection, files in item.items():
            basedir = Path(config['collections'][collection]['path'])
            for pattern in files:
                glob = list(basedir.glob(pattern))
                if len(glob) > 1:
                    print(f'Pattern {pattern}')


print('')