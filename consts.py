from pathlib import Path

TESTFILEDIR = Path.cwd() / 'testfiles'
TESTFILE = TESTFILEDIR / '250-18_GeoTiffv1-02.tif'
SRCDIR_250 = Path('/Volumes/Tortoise/Topo/250')
SRCDIR_50 = Path('/Volumes/Tortoise/Topo/50')
SRC_CRS = 'EPSG:2193'
DST_CRS = 'EPSG:3857'
Z250 = [6, 7, 8, 9, 10, 11, 12, 13]
Z50 = [14, 15]
SHEETS = {
    1: {
        'AS21',
        'AT24', 'AT25',
        'AU25',
        'AV25pt'
    },
    2: {
        'AU26', 'AU27', 'AU28pt', 'AU29pt'
        'AV26', 'AV27', 'AV28', 'AV29', 'AV30',
        'AW26', 'AW27', 'AW28', 'AW29', 'AW30',
    },
    3: {
        'AW31', 'AW32',
        'AX31', 'AX32pt', 'AX33',
        'AY31', 'AY32', 'AY33', 'AY34',
        'AZ31', 'AZ32', 'AZ34', 'AZ35'

    },
    4: {
        'AX27', 'AX28', 'AX29', 'AX30',
        'AY28', 'AY29', 'AY30',
        'AZ29', 'AZ30',
        'BA30',
        'BB30pt'
    },
    5: {
        'BA31', 'BA32', 'BA33', 'BA34', 'BA35'
        'BB31', 'BB32', 'BB33', 'BB34', 'BB35'
        'BC31', 'BC32', 'BC33', 'BC34', 'BC35'
        'BD31pt', 'BD32', 'BD33', 'BD34', 'BD35'
        'BE31', 'BE32', 'BE33', 'BE34', 'BE35'
    },
    6: {
        'BA36pt',
        'BB36', 'BB37pt',
        'BC36', 'BC37', 'BC40pt',
        'BD36', 'BD37', 'BD38', 'BD39pt', 'BD40pt',
        'BE36', 'BE37', 'BE38', 'BE39', 'BE40'
    },
    7: {
        'BC40pt'
        'BD42', 'BD43', 'BD44', 'BD45',
        'BE31', 'BE32', 'BE33', 'BE34', 'BE35'
    },
    8: {
        'BG30pt',
        'BH28', 'BH29', 'BH30',
        'BJ28', 'BJ29', 'BJ30',
        'BK28pt', 'BK29', 'BK30',
    },
    9: {
        'BF31', 'BF32', 'BF33', 'BF34', 'BF35'
        'BG31', 'BG32', 'BG33', 'BG34', 'BG35'
        'BH31', 'BH32', 'BH33', 'BH34', 'BH35'
        'BJ31', 'BJ32', 'BJ33', 'BJ34', 'BJ35'
        'BK31', 'BK32', 'BK33', 'BK34', 'BK35'
    },
    10: {
        'BF36', 'BF37', 'BF38', 'BF39', 'BF40'
        'BG36', 'BG37', 'BG38', 'BG39', 'BG40'
        'BH36', 'BH37', 'BH38', 'BH39', 'BH40'
        'BJ36', 'BJ37', 'BJ38', 'BJ39', 'BJ40pt'
        'BK36', 'BK37', 'BK38', 'BK39', 'BK40pt'
    },
    11: {
        'BF41', 'BF42', 'BF43', 'BF44', 'BF45pt'
        'BG41', 'BG42', 'BG43', 'BG44',
        'BH41', 'BH42', 'BH43',
        'BJ43pt'
    },
    12: {
        'BM24pt', 'BM25pt',
        'BN22', 'BN23', 'BN24', 'BN25',
        'BP22', 'BP23', 'BP24', 'BP25',
        'BQ21pt', 'BQ22', 'BQ23', 'BQ24', 'BQ25',
    },
    13: {
        'BN28', 'BN29pt',
        'BP26pt', 'BP27', 'BP28', 'BP29', 'BP30pt',
        'BQ26', 'BQ27', 'BQ28', 'BQ29'

    },
    14: {
        'BL31PT', 'BL32', 'BL33', 'BL34', 'BL35',
        'BM33', 'BM34', 'BM35',
        'BN32pt', 'BN33', 'BN34', 'BN35',
    },
    15: {
        'BL36', 'BL37', 'BL38', 'BL39',
        'BM36', 'BM37', 'BM38', 'BM39pt',
        'BN36', 'BN37', 'BN38pt',
        'BP36',
        'BQ36PT'
    },
    16: {
        'BR33', 'BR34'
    },
    17: {
        'BR20',
        'BS19', 'BS20',
        'BT19', 'BT20',
        'BU18', 'BU19', 'BU20',
        'BV16', 'BV17', 'BV18', 'BV19', 'BV20',
    },
    18: {
        'BR21', 'BR22', 'BR23', 'BR24', 'BR25',
        'BS21', 'BS22', 'BS23', 'BS24', 'BS25',
        'BT21', 'BT22', 'BT23', 'BT24', 'BT25',
        'BU21', 'BU22', 'BU23', 'BU24', 'BU25',
        'BV21', 'BV22', 'BV23', 'BV24', 'BV25',
    },
    19: {
        'BR26', 'BR27', 'BR28', 'BR29',
        'BS26', 'BS27', 'BS28', 'BS29',
        'BT26', 'BT27', 'BT28',
        'BU26', 'BU27',
        'BV26',
    },
    20: {
        'BY10pt',
        'BZ09', 'BZ10',
        'CA07pt', 'CA08', 'CA09', 'CA10'
    },
    21: {
        'BW14pt', 'BW15',
        'BX12pt', 'BX13', 'BX14', 'BX15',
        'BY11', 'BY12', 'BY13', 'BY14', 'BY15',
        'BZ11', 'BZ12', 'BZ13', 'BZ14', 'BZ15',
        'CA11', 'CA12', 'CA13', 'CA14', 'CA15',
    },
    22: {
        'BW16', 'BW17', 'BW18', 'BW19', 'BW20',
        'BX16', 'BX17', 'BX18', 'BX19', 'BX20',
        'BY16', 'BY17', 'BY18', 'BY19', 'BY20',
        'BZ16', 'BZ17', 'BZ18', 'BZ19', 'BZ20', 'BZ21pt',
        'CA16', 'CA17', 'CA18', 'CA19',
    },
    23: {
        'BW21', 'BW22', 'BW23', 'BW24', 'BW25pt',
        'BX21', 'BX22', 'BX23', 'BX24', 'BX25',
        'BY21', 'BY22', 'BY23', 'BY24', 'BY25',
        'BZ21pt'
    },
    24: {
        'CC05',
        'CD04pt', 'CD05',
        'CE04', 'CE05',
        'CF04', 'CF05'
    },
    25: {
        'CB06', 'CB07', 'CB08', 'CB09', 'CB10',
        'CC06', 'CC07', 'CC08', 'CC09', 'CC10',
        'CD06', 'CD07', 'CD08', 'CD09', 'CD10',
        'CE06', 'CE07', 'CE08', 'CE09', 'CE10',
        'CF06', 'CF07', 'CF08', 'CF09', 'CF10',
    },
    26: {
        'CB11', 'CB12', 'CB13', 'CB14', 'CB15',
        'CC11', 'CC12', 'CC13', 'CC14', 'CC15',
        'CD11', 'CD12', 'CD13', 'CD14', 'CD15',
        'CE11', 'CE12', 'CE13', 'CE14', 'CE15',
        'CF11', 'CF12', 'CF13', 'CF14', 'CF15',
    },
    27: {
        'CB16', 'CB17', 'CB18', 'CB19',
        'CC16', 'CC17', 'CC18', 'CC19pt',
        'CD16', 'CD17', 'CD18',
        'CE16', 'CE17', 'CE18',
        'CF16',
    },
    28: {
        'CE04', 'CE05', 'CE06', 'CE07', 'CE08',
        'CF04', 'CF05', 'CF06', 'CF07', 'CF08',
        'CG05', 'CG06', 'CG07pt', 'CG08',
        'CH05CH06', 'CH08',
        'CJ07CK07', 'CJ08'
    },
    29: {
        'CG06', 'CG07pt', 'CG08', 'CG09', 'CG10',
        'CH08', 'CH09', 'CH10',
        'CJ07CK07', 'CJ08', 'CJ09', 'CJ10',
        'CK08'
    },
    30: {
        'CG11', 'CG12', 'CG13', 'CG14', 'CG15',
        'CH11', 'CH12', 'CH13'
    }
}