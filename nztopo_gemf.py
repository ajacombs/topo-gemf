import io
import os
import re
import subprocess
import time
import copy
import multiprocessing
from pathlib import Path
from collections import defaultdict
from operator import itemgetter

import mercantile
from mercantile import Tile
import numpy as np
import rasterio
from rasterio.io import MemoryFile, DatasetReader
from rasterio._io import virtual_file_to_buffer
import rasterio.transform
from rasterio.warp import calculate_default_transform, transform, reproject, Resampling

from tqdm import tqdm

from consts import TESTFILEDIR, TESTFILE, SRCDIR_250, SRCDIR_50, DST_CRS, SHEETS, Z250, Z50

src_250 = None
src_50 = None
base_kwds = None
sheet = None

# src_250 = rasterio.open(str(TESTFILE))
#
# transform, width, height = calculate_default_transform(
#     src_250.crs, DST_CRS, src_250.width, src_250.height, *src_250.bounds)
# kwargs = src_250.meta.copy()
# kwargs.update({
#     'crs': DST_CRS,
#     'transform': transform,
#     'width': width,
#     'height': height,
#     'count': 4,
#     'driver': 'GTiff',
# })
#
# with rasterio.open(str(TESTFILEDIR / 'test250warp.tif'), 'w', **kwargs) as dst_250:
#     src_bands = [rasterio.band(src_250, band) for band in src_250.indexes] + \
#                 [np.full(shape=src_250.shape, fill_value=255, dtype=np.uint8)]
#     dst_bands = [rasterio.band(dst_250, band) for band in dst_250.indexes]
#     for i in range(4):
#         reproject(
#             source=src_bands[i],
#             destination=dst_bands[i],
#             src_transform=src_250.transform,
#             src_crs=src_250.crs,
#             dst_transform=dst_250.transform,
#             dst_crs=dst_250.crs,
#             resampling=Resampling.bilinear,
#             num_threads=8
#         )
#
#
# def build_50_vrt(sheet, src_bounds):
#
#     args = [
#         'gdalbuildvrt',
#         '-te',
#             str(src_bounds.left),  # xmin
#             str(src_bounds.bottom),  # ymin
#             str(src_bounds.right),  # xmax
#             str(src_bounds.top),  # ymax
#         '-addalpha',
#         '-a_srs', 'EPSG:2193',
#         str(TESTFILEDIR / (str(sheet) + '.vrt'))
#     ]
#
#     for file in SRCDIR_50.iterdir():
#         sheetname = re.match(r'[a-z0-9\-]+', str(file.name), flags=re.I).group(0)
#         if sheetname in SHEETS[sheet]:
#             args.append(str(file))
#
#     subprocess.call(args)
#
# build_50_vrt(18, src_250.bounds)
#
# src_50 = rasterio.open(str(TESTFILEDIR / '18.vrt'))
#
# transform, width, height = calculate_default_transform(
#     src_50.crs, DST_CRS, src_50.width, src_50.height, *src_50.bounds)
# kwargs = src_50.meta.copy()
# kwargs.update({
#     'crs': DST_CRS,
#     'transform': transform,
#     'width': width,
#     'height': height,
#     'count': 4,
#     'driver': 'GTiff',
# })
#
#
# start = time.perf_counter()
# with rasterio.open(str(TESTFILEDIR / 'test50warp.tif'), 'w', **kwargs) as dst_50:
#     src_bands = [rasterio.band(src_50, band) for band in src_50.indexes]
#                 # [np.full(shape=src_50.shape, fill_value=255, dtype=np.uint8)]
#     dst_bands = [rasterio.band(dst_50, band) for band in dst_50.indexes]
#     for i in range(4):
#         print('Warping band ', i+1)
#         reproject(
#             source=src_bands[i],
#             destination=dst_bands[i],
#             src_transform=src_50.transform,
#             src_crs=src_50.crs,
#             dst_transform=dst_50.transform,
#             dst_crs=dst_50.crs,
#             resampling=Resampling.bilinear,
#             num_threads=8
#         )
# end = time.perf_counter()
# elapsed = round(end - start, 1)
# print('50:', elapsed)
#
# src_250.close()
# src_50.close()


def load_250_gtiff(sheet):
    for filepath in SRCDIR_250.iterdir():
        filenum = re.match(r'250-(\d+)', str(filepath.name)).group(1)
        if int(filenum) == sheet:
            diskfile = rasterio.open(str(filepath))
            profile = diskfile.profile
            profile.data['count'] = 4
            memfile = MemoryFile()
            with memfile.open(**profile) as tmp:
                tmp.write(diskfile.read(), (1,2,3))
                alpha = np.full(shape=tmp.shape, fill_value=255, dtype=np.uint8)
                tmp.write(alpha, 4)
            return memfile
    else:
        print('Cannot find sheet {}'.format(sheet))


def load_50_vrt(sheet, src_bounds):
    vrtfile = TESTFILEDIR / (str(sheet) + '.vrt')
    args = [
        'gdalbuildvrt',
        '-te',
        str(src_bounds.left),  # xmin
        str(src_bounds.bottom),  # ymin
        str(src_bounds.right),  # xmax
        str(src_bounds.top),  # ymax
        '-addalpha',
        '-a_srs', 'EPSG:2193',
        str(vrtfile)
    ]
    for file in SRCDIR_50.iterdir():
        sheetname = re.match(r'[a-z0-9\-]+', str(file.name), flags=re.I).group(0)
        if sheetname in SHEETS[sheet]:
            args.append(str(file))
    subprocess.call(args, stdout=subprocess.PIPE)
    return vrtfile


def process_tile(tile):
    if tile.z in Z250:
        src = src_250.open()
    if tile.z in Z50:
        src = rasterio.open(str(src_50))
    # Get the bounds of the tile.
    ulx, uly = mercantile.xy(
        *mercantile.ul(tile.x, tile.y, tile.z))
    lrx, lry = mercantile.xy(
        *mercantile.ul(tile.x + 1, tile.y + 1, tile.z))

    kwds = base_kwds.copy()
    kwds['transform'] = rasterio.transform.from_bounds(ulx, lry, lrx, uly, 256, 256)

    with rasterio.open('/vsimem/tileimg', 'w', **kwds) as tmp:
        _input = rasterio.band(src, src.indexes)
        _output = rasterio.band(tmp, tmp.indexes)
        reproject(_input, _output, num_threads=1, resampling=Resampling.bilinear)
    src.close()

    data = bytearray(virtual_file_to_buffer('/vsimem/tileimg'))

    args = [
        'pngquant',
        '--speed', '3',  # speed from 1 (slowest) to 10 (fastest)
        '-'  # read from stdin and output to stdout
    ]
    quant = subprocess.run(args, input=data, stdout=subprocess.PIPE)
    data = bytearray(quant.stdout)

    return tile, data


def main(sheets):
    global sheet
    for sheet in sheets:
        global src_250, src_50, base_kwds
        src_250 = load_250_gtiff(sheet)
        sheet_250 = src_250.open()
        src_50 = load_50_vrt(sheet, sheet_250.bounds)
        base_kwds = {
            'driver': 'PNG',
            'dtype': 'uint8',
            'height': 256,
            'width': 256,
            'count': 4,
            'crs': DST_CRS
        }
        # Compute the geographic bounding box of the sheet.
        west, south, east, north = rasterio.warp.transform_bounds(
            src_crs=sheet_250.crs, dst_crs='EPSG:4326',
            left=sheet_250.bounds.left,
            bottom=sheet_250.bounds.bottom,
            right=sheet_250.bounds.right,
            top=sheet_250.bounds.top
        )

        (west_old, east_old), (south_old, north_old) = transform(
            src_crs=sheet_250.crs, dst_crs='EPSG:4326',
            xs=(sheet_250.bounds.left, sheet_250.bounds.right),
            ys=(sheet_250.bounds.bottom, sheet_250.bounds.top)
        )
        sheet_250.close()

        # Constrain bounds.
        # EPS = 1.0e-10
        # west = max(-180 + EPS, west)
        # south = max(-85.051129, south)
        # east = min(180 - EPS, east)
        # north = min(85.051129, north)

        minzoom = min(Z250 + Z50)
        maxzoom = max(Z250 + Z50)

        tiles_new = set(mercantile.tiles(
            west, south, east, north, range(minzoom, maxzoom + 1)))

        tiles_old = set(mercantile.tiles(
            west_old, south_old, east_old, north_old, range(minzoom, maxzoom + 1)))

        tiles = list(tiles_new - tiles_old)

        tiles_old_dict = defaultdict(list)

        for tile in tiles_old:
            tiles_old_dict[tile.x].append(tile)

        for x, tilelist in tiles_old_dict.items():
            sortlist = sorted(tilelist, key=itemgetter(1))
            tiles.append(sortlist[0])





        pool = multiprocessing.Pool(
            # processes=8,
        )

        pbar = tqdm(total=len(tiles), desc='Sheet {}'.format(sheet), unit='tile',)

        for tile, data in pool.imap(process_tile, tiles):
            quantfile = Path(
                TESTFILEDIR / str(sheet) / str(tile.z) / str(tile.x) / (
                str(tile.y) + '.png')
            )
            os.makedirs(str(quantfile.parent), exist_ok=True)
            pbar.update(1)
            with quantfile.open('wb') as f:
                f.write(bytes(data))
        pool.close()
        pbar.close()

main(range(1,31))